package net.jomcraft.extrablocksy;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ExtraBlocksyTab extends CreativeTabs {

	public ExtraBlocksyTab() {
		super("extraBlocksyTab");
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(ExtraBlocksy.lampOne);
	}

}
