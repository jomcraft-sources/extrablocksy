package net.jomcraft.extrablocksy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.material.Material;

@Mod(modid = ExtraBlocksy.MODID, name = ExtraBlocksy.NAME, version = ExtraBlocksy.VERSION)
public class ExtraBlocksy {

	public static final String MODID = "extrablocksy";
	public static final String NAME = "ExtraBlocksy";
	public static final String VERSION = "EB-Version";
	public static final Logger log = LogManager.getLogger(ExtraBlocksy.MODID);
	public static BlockLampOne lampOne;
	public static BlockLampOneLit lampOneLit;
	public static BlockLampTwo lampTwo;
	public static BlockLampTwoLit lampTwoLit;
	public static BlockNoteOne noteOne;
	public static BlockNoteTwo noteTwo;
	public static ExtraBlocksyTab extraBlocksyTab = new ExtraBlocksyTab();
	
	@Instance
	public static ExtraBlocksy instance;
	
	public ExtraBlocksy() {
		instance = this;
	}

	@EventHandler
	public static void preInit(FMLPreInitializationEvent event) {
		lampOne = (BlockLampOne) new BlockLampOne("blocklampone", Material.redstoneLight).setBlockName("blocklampone");
		lampOneLit = (BlockLampOneLit) new BlockLampOneLit("blocklampone_lit", Material.redstoneLight).setBlockName("blocklampone_lit");
		lampTwo = (BlockLampTwo) new BlockLampTwo("blocklamptwo", Material.redstoneLight).setBlockName("blocklamptwo");
		lampTwoLit = (BlockLampTwoLit) new BlockLampTwoLit("blocklamptwo_lit", Material.redstoneLight).setBlockName("blocklamptwo_lit");
		noteOne = (BlockNoteOne) new BlockNoteOne("blocknoteone", Material.wood).setBlockName("blocknoteone");
		noteTwo = (BlockNoteTwo) new BlockNoteTwo("blocknotetwo", Material.wood).setBlockName("blocknotetwo");
	}
	
	@EventHandler
	public static void init(FMLInitializationEvent event) {
		GameRegistry.registerBlock(lampOne, ItemLamp.class, "blocklampone");
		GameRegistry.registerBlock(lampOneLit, ItemLamp.class, "blocklampone_lit");
		GameRegistry.registerBlock(lampTwo, ItemLamp.class, "blocklamptwo");
		GameRegistry.registerBlock(lampTwoLit, ItemLamp.class, "blocklamptwo_lit");
		GameRegistry.registerBlock(noteOne, ItemNote.class, "blocknoteone");
		GameRegistry.registerTileEntity(TileEntityNote.class, "blocknote");
		GameRegistry.registerBlock(noteTwo, ItemNote.class, "blocknotetwo");
	}
	
	public static ExtraBlocksy getInstance() {
		return instance;
	}
}
