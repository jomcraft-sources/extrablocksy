package net.jomcraft.extrablocksy;

import net.minecraft.block.*;
import net.minecraft.util.*;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.*;
import net.minecraft.block.material.*;
import net.minecraft.creativetab.*;
import net.minecraft.item.*;
import net.minecraft.tileentity.TileEntity;

import java.util.*;
import net.minecraft.client.renderer.texture.*;

public class BlockNoteOne extends Block {
	
    private static final IIcon[] icons = new IIcon[16];

    protected BlockNoteOne(final String unlocalizedName, final Material material) {
        super(material);
        this.setBlockName(unlocalizedName);
        this.setBlockTextureName("extrablocksy:note_one");
        this.setHardness(0.8F);
        this.setCreativeTab(ExtraBlocksy.extraBlocksyTab);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister p_149651_1_) {
        for (int i = 0; i < icons.length; ++i) {
        	icons[i] = p_149651_1_.registerIcon("extrablocksy:note_one_" + func_149997_b(i));
        }
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
        return icons[p_149691_2_ % icons.length];
    }
    
    @SideOnly(Side.CLIENT)
    public static int func_149997_b(int p_149997_0_) {
        return p_149997_0_ & 15;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(final Item par1, final CreativeTabs par2CreativeTabs, final List par3List) {
        for (int j = 0; j < icons.length; ++j) {
            par3List.add(new ItemStack(par1, 1, j));
        }
    }
    
    @Override
    public int damageDropped(int p_149692_1_) {
    	return p_149692_1_;
    }
    
    @Override
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
        return Item.getItemFromBlock(ExtraBlocksy.noteOne);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public Item getItem(World p_149694_1_, int p_149694_2_, int p_149694_3_, int p_149694_4_) {
        return Item.getItemFromBlock(ExtraBlocksy.noteOne);
    }

    @Override
    protected ItemStack createStackedBlock(int p_149644_1_) {
        return new ItemStack(ExtraBlocksy.noteOne, 1, p_149644_1_);
    }
    
    @Override
    public void onNeighborBlockChange(World p_149695_1_, int p_149695_2_, int p_149695_3_, int p_149695_4_, Block p_149695_5_)
    {
        boolean flag = p_149695_1_.isBlockIndirectlyGettingPowered(p_149695_2_, p_149695_3_, p_149695_4_);
        
        int meta = p_149695_1_.getBlockMetadata(p_149695_2_, p_149695_3_, p_149695_4_);	
        
        TileEntityNote tileentitynote = (TileEntityNote)p_149695_1_.getTileEntity(p_149695_2_, p_149695_3_, p_149695_4_);

        if (tileentitynote != null && tileentitynote.previousRedstoneState != flag)
        {
            if (flag)
            {
            	p_149695_1_.playSoundEffect((double)p_149695_2_ + 0.5D, (double)p_149695_3_ + 0.5D, (double)p_149695_4_ + 0.5D, "extrablocksy:note_one_" + meta, 3.0F, 1.0F);
            }

            tileentitynote.previousRedstoneState = flag;
        }
    }
    
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
    	return new TileEntityNote();
    }

    @Override
    public boolean hasTileEntity(int metadata) {
    	return true;
    }
}